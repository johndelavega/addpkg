package addpkg

import "fmt"

const mVersion = "v0.1.0" // semver.org Semantic Versioning 2.0.0

// Add two integer numbers
func Add(a int, b int) int {

	return a + b
}

// PackageInfo returns info string
func PackageInfo() string {

	return fmt.Sprintf("addpkg Version %s | gitlab.com/johndelavega/addpkg", mVersion)

}

// Version returns version string for debugging
func Version() string {

	return fmt.Sprintf("%s", mVersion)

}
